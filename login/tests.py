from django.test import TestCase,Client
from .views import login

# Create your tests here.
class UnitTest(TestCase):
    #Test untuk cek url login
    def test_url_address_login_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    #Test untuk cek alamat url login tidak ada
    def test_url_address_login_is_not_exist(self):
        response = Client().get('/')
        self.assertNotEqual(response.status_code, 200)

    #Test untuk cek fungsi login telah memiliki isi
    def test_login_is_written(self):
        self.assertIsNotNone(login)

    #Test untuk cek template html telah digunakan pada halaman login
    def test_template_html_login_is_exist(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')
